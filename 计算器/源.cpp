#include<iostream>
#include"Solution.h"
using namespace std;

int main() 
{
	Solution so;            //Solution类实例化
	double a, b;
	char ch;                //定义待输入变量
	cin >> a >> ch >> b;    //输入运算数和运算符号
	cout << so.calculator(a, ch, b) << endl;    //调用Solution类的计算器函数并输出

	return 0;
	system("pause");
}