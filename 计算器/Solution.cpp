#pragma once                    
#include"Solution.h"

double Solution::calculator(double a, char ch, double b)
{
	if (ch == '+')              //对运算符进行识别，进行相应计算
	{
		return a + b;
	}
	else if (ch == '-')
	{
		return a - b;
	}
	else if (ch == '*')
	{
		return a * b;
	}
	else if (ch == '/')
	{
		if (b == 0)            //除法计算时分母为0报错
		{
			return ERROR;
		}
		else
		{
			return a / b;
		}
	}
	else                       //运算符参数非设定的四则运算之一，返回ERROR
	{
		return ERROR;
	}
}